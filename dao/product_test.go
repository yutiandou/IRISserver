package dao

import (
	"fmt"
	"log"
	"project/ProductServer/model"
	_ "project/ProductServer/util"

	"testing"
)

func TestProduct(t *testing.T) {
	product := &model.Product{
		ProductName:  "haha",
		ProductNum:   10,
		ProductImage: "ssss",
		ProductURL:   "www",
	}
	id := InsertProduct(product)
	if id == 0 {
		log.Fatalln("InsertProduct faile")
	} else {
		fmt.Println("InsertProduct success")
	}
	product.ProductName = "hehe"
	product.ID = id
	if UpdateProduct(product) {
		log.Println("UpdateProduct success")
	}
	pros := SelectProducts(0, 100)
	if pros != nil {
		fmt.Println(pros[0])
	}
}

package main

import (
	"fmt"
	"project/wd_rabbitmq/rabbitmq"
)

//简单模式
// func main() {
// 	rbmq := rabbitmq.NewRabbitMQSimple("wdtest")
// 	rbmq.PublishSimple("hello world")
// 	fmt.Println("发送成功")
// }

//订阅模式
// func main() {
// 	rbmq := rabbitmq.NewRabbitMQPubSub("wdexchange")
// 	rbmq.PublishSub("hello world")
// 	fmt.Println("发送成功")
// }

func main() {
	rbmq := rabbitmq.NewRabbitMQRouting("wdexchange", "one")
	rbmq.PublishRouting("hello one i aim wangdong")
	rbmq.Close()
	rbmqt := rabbitmq.NewRabbitMQRouting("wdexchange", "two")
	rbmqt.PublishRouting("hello two i aim wangdong")
	rbmqt.Close()
	fmt.Println("发送成功")
}

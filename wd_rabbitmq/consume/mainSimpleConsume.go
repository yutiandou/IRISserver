package main

import (
	"fmt"
	"project/wd_rabbitmq/rabbitmq"
)

//简单模式
// func main() {
// 	rbmq := rabbitmq.NewRabbitMQSimple("wdtest")
// 	rbmq.ConsumeSimple()
// }
func main() {
	fmt.Printf("请输入参数--》")
	var offset int
	fmt.Scan(&offset)
	if offset == 1 {
		rbmq := rabbitmq.NewRabbitMQRouting("wdexchange", "one")
		rbmq.ConsumeRouting()
	} else {
		rbmqt := rabbitmq.NewRabbitMQRouting("wdexchange", "two")
		rbmqt.ConsumeRouting()
	}
}

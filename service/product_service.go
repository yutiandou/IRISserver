package service

import (
	"project/ProductServer/dao"
	"project/ProductServer/model"
)

//IProductService ..业务逻辑抽象
type IProductService interface {
	GetProductByID(uint32) *model.Product
	GetAllProduct(uint32, uint32) []*model.Product
	DeleteProductByID(uint32) bool
	InsertProduct(product *model.Product) uint32
	UpdateProduct(product *model.Product) bool
}

//ProductService ..业务逻辑实体
type ProductService struct{}

//NewProductService ..创建一个业务实体
func NewProductService() IProductService {
	return &ProductService{}
}

//GetProductByID ..
func (p *ProductService) GetProductByID(productID uint32) *model.Product {
	return dao.SelectProduct(productID)
}

//GetAllProduct ..
func (p *ProductService) GetAllProduct(pid, conut uint32) []*model.Product {
	return dao.SelectProducts(pid, conut)
}

//DeleteProductByID ..
func (p *ProductService) DeleteProductByID(pid uint32) bool {
	return dao.DeleteProduct(pid)
}

//InsertProduct ..
func (p *ProductService) InsertProduct(product *model.Product) (pid uint32) {
	pid = dao.InsertProduct(product)
	return
}

//UpdateProduct ..
func (p *ProductService) UpdateProduct(product *model.Product) bool {
	return dao.UpdateProduct(product)
}

package util

import (
	"log"
)

//    // 控制输出日志信息的细节，不能控制输出的顺序和格式。
//     // 输出的日志在每一项后会有一个冒号分隔：例如2009/01/23 01:23:23.123123 /a/b/c/d.go:23: message
//     Ldate         = 1 << iota     // 日期：2009/01/23
//     Ltime                         // 时间：01:23:23
//     Lmicroseconds                 // 微秒级别的时间：01:23:23.123123（用于增强Ltime位）
//     Llongfile                     // 文件全路径名+行号： /a/b/c/d.go:23
//     Lshortfile                    // 文件名+行号：d.go:23（会覆盖掉Llongfile）
//     LUTC                          // 使用UTC时间
//     LstdFlags     = Ldate | Ltime // 标准logger的初始值

func init() {
	//设置自定义的输出前缀
	log.SetPrefix("WD--->")
	log.SetFlags(log.Ldate | log.Ltime | log.Llongfile)
	// outfile, err := os.OpenFile("filepaht", os.O_CREATE, os.O_WRONLY, os.O_APPEND, 0644)
	// if err != nil {
	// 	panic("log file open fail!!!")
	// }
	// log.SetOutput(outfile)

}

package model

//Product ..商品实体
type Product struct {
	ID           uint32 `json:"id" db:"ID" imooc:"ID"`
	ProductName  string `json:"ProductName" db:"productName" imooc:"ProductName"`
	ProductNum   uint32 `json:"ProductNum" db:"productNum" imooc:"ProductNum"`
	ProductImage string `json:"ProductImage" db:"productImage" imooc:"ProductImage"`
	ProductURL   string `json:"ProductUrl" db:"productUrl" imooc:"ProductUrl"`
}

package main

import (
	"project/ProductServer/backend/web/controller"
	_ "project/ProductServer/util"

	"github.com/gin-gonic/gin"
	//"github.com/opentracing/opentracing-go/log"
)

func main() {
	app := gin.Default()
	app.Static("/assets", "./web/assets")
	//gin.SetMode(gin.ReleaseMode)
	//加载模板
	app.LoadHTMLGlob("web/views/*")

	app.GET("/", controller.IndexGetAllProduct)
	app.GET("/product/all", controller.GetAllProduct)
	app.GET("/product/manager", controller.GetUpdateProduct)
	app.GET("/product/add", controller.GetAddProduct)
	app.GET("/product/delete", controller.GetDeleteProduct)

	//app.POST("/product/update", controller.PostUpdateProduct)
	app.POST("/product/add", controller.PostAddProduct)

	app.Run(":8000")
}
